docker run \
	-d \
	-p 8080:8080 \
	-v jenkins-data:/var/jenkins_home \
	--name jenkins \
	jenkinsci/blueocean:latest	
